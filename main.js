const container = document.querySelector('.container');
const filmURL = "https://ajax.test-danit.com/api/swapi/films";
const cards = [];
const configElements = {cardHeaderEl: ['h3','card-header'], cardEl:['div',"card"], episodeIdEl:['p','card-episode-id'], titleEl:['p','card-title'], opening_crawlEl:['p','card-opening'], charactersEl:['div',
    'card-characters'], charHeaderEl:['h4','card-character-header'] };

class Card{
    constructor(configElements, parent, id, episodeId, title, opening_crawl, charUrlArr){
        this.filmId = id;
        this.episodeId = episodeId;
        this.title = title;
        this.opening_crawl = opening_crawl;
        this.charUrlArr = charUrlArr;
        const keys = Object.keys(configElements);
        this.DOMelements = {
            parent:         parent,
                };
        keys.forEach(key=> {
            this.DOMelements[key] = document.createElement(`${configElements[key][0]}`);
            this.DOMelements[key].className = `${configElements[key][1]}`;
        });
        console.log(this.DOMelements);

        this.elementsInit();
    }

    elementsInit(){
        const {parent, cardEl, cardHeaderEl, episodeIdEl, titleEl, opening_crawlEl, charactersEl, charHeaderEl} = this.DOMelements;
        const {filmId, episodeId, title, opening_crawl, charUrlArr} = this;

        cardHeaderEl.innerText = `Краткое описание фильма (id: ${filmId})`;
        // cardEl.className = "card";
        episodeIdEl.innerText = `Номер эпизода:   ${episodeId}`;
        titleEl.innerText = `Название фильма:   ${title}`;
        opening_crawlEl.innerText = `Краткое содержание:  ${opening_crawl}`;
        charHeaderEl.innerText = `Список персонажей: `;

        // charactersEl.className = "character";   //TODO сюда будем закидывать списки персонажей!
        charactersEl.dataset.name = "film-id";     //TODO сюда будем закидывать списки персонажей!
        charactersEl.append(charHeaderEl);
        cardEl.append(cardHeaderEl, episodeIdEl, titleEl, opening_crawlEl, charactersEl);
        parent.append(cardEl);
    }

     getCharacters(){
        const {charUrlArr} = this;
        const fetchArr = charUrlArr.map( url=> fetch(url) );
        Promise.all(fetchArr)
            .then(arr => Promise.all(arr.map( res => res.json() )) )
            .then(charArr => charArr.forEach( char=> this.putCharacter(char)));
    }

     putCharacter({name}){
         const {charactersEl} = this.DOMelements;
         const newCharEl = document.createElement('p');
         newCharEl.innerText = name;
         charactersEl.append(newCharEl);
     }

}


fetch(filmURL).then(res=>res.json()).then(films=>{

            films.forEach(film=>{
                const {id, episodeId, name, openingCrawl, characters} = film;
                //вывели всё на экран
                cards[id] = new Card(configElements, container, id, episodeId, name, openingCrawl, characters);
                cards[id].getCharacters();
            });





    }
);
